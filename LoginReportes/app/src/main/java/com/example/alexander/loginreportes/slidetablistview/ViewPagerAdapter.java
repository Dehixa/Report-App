package com.example.alexander.loginreportes.slidetablistview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.alexander.loginreportes.slindingtab.historias.Historias;
import com.example.alexander.loginreportes.slindingtab.misCasos.Casos;
import com.example.alexander.loginreportes.slindingtab.perfil.Perfil;

/**
 * Created by cesar on 27/12/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter{
    CharSequence Titles[];
    int NumbOfTabs;

    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[],
                            int mNumbOfTabsumb) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
    }
    // This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Perfil();
            case 1:
                return new Casos();
            case 2:
                return new Historias();
        }
        return null;
    }
    // This method return the titles for the Tabs in the Tab Strip
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }
    // This method return the Number of tabs for the tabs Strip
    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
