package com.example.alexander.loginreportes.slindingtab.historias;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexander.loginreportes.R;
/**
 * Created by cesar on 27/12/2017.
 */

public class Historias extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.historias, container, false);
    }



}
