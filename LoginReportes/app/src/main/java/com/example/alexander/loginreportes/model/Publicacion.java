package com.example.alexander.loginreportes.model;

/**
 * Created by dehixa on 02/01/18.
 */

public class Publicacion {
    // Atributos
    private String usuario;
    private String descripcion;
    private String fecha;

    public Publicacion() {}

    public Publicacion(String usuario, String descripcion, String fecha) {
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}