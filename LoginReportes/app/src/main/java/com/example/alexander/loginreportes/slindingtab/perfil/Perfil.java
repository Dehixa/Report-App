package com.example.alexander.loginreportes.slindingtab.perfil;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexander.loginreportes.R;

/**
 * Created by cesar on 27/12/2017.
 */

public class Perfil extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.envio_reporte, container, false);

        return v;
    }
}
