package com.example.alexander.loginreportes.model.sql;

/**
 * Created by dehixa on 03/01/18.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexionDB extends AsyncTask<String,Void,ResultSet> {

    @Override
    protected ResultSet doInBackground(String... strings) {

        try {
            Connection conn;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://192.168.1.106:3306/casos", "root", "");

            Statement estado = conn.createStatement();
            System.out.println("Conexion establecida");

            String peticion ="select * from caso";
            ResultSet result = estado.executeQuery(peticion);
            return result;
        } catch (SQLException e) {
            Log.d("sql", e.getMessage());
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            Log.d("class", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultSet result) {

        try {
            if (result==null ) {
                System.out.println("No data");
            }else{
                /*tvGenero.setText(result.getString("genero"));
                tvValoracion.setText(Float.toString(result.getFloat("valoracion")));
                tvPEGI.setText(Integer.toString(result.getInt("PEGI")));
                tvPrecio.setText(Float.toString(result.getFloat("precio")));*/
                System.out.println("user" + result.getString("user"));
                System.out.println("desc" + result.getString("descripcion"));
                System.out.println("fecha" + result.getString("fecha"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}

/*public class ConexionDB {
    Connection con=null;
    public Connection conexion(){
        try {
            //cargar nuestro driver
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost/casos","root","");
            System.out.println("conexion establecida");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("error de conexion");
        }
        return con;
    }
}*/