package com.example.alexander.loginreportes.adapters;

/**
 * Created by dehixa on 02/01/18.
 */

import android.content.Context;
import android.widget.ArrayAdapter;

import com.example.alexander.loginreportes.model.sql.ConexionDB;

public class PublicacionAdapter extends ArrayAdapter{

    public PublicacionAdapter(Context context) {
        super(context, 0);

        ConexionDB cc = new ConexionDB();
        cc.execute();

    }

}

/*public class PublicacionAdapter extends ArrayAdapter {

    // Atributos
    JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://127.0.0.1/casos/datos.json";
    private static final String TAG = "PostAdapter";
    List<Publicacion> items;

    public PublicacionAdapter(Context context) {
        super(context,0);


        // Nueva petición JSONObject
        jsArrayRequest = new JsonObjectRequest(Request.Method.GET,
                URL_BASE,
                (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        items = parseJson(response);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Respuesta en JSON: " + error.getMessage());

                    }
                }
        );

        // Añadir petición a la cola
        ColaSingleton.getInstance(getContext()).addToRequestQueue(jsArrayRequest);

        System.out.println(items);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        //Comprobando si el View no existe

        if (view == null){
            view = layoutInflater.inflate(
                    R.layout.item_publicacion,
                    parent,
                    false);
        }


        // Obtener el item_publicacion actual
        Publicacion item = items.get(position);

        // Obtener Views
        TextView textoUsuario = (TextView) view.findViewById(R.id.usuario);
        TextView textoFecha = (TextView) view.findViewById(R.id.fecha);
        TextView textoDescripcion = (TextView) view.findViewById(R.id.descripcion);

        Log.d("user:",item.getUsuario());
        Log.d("fecha:",item.getFecha());
        Log.d("desc:",item.getDescripcion());

        // Actualizar los Views
        textoUsuario.setText(item.getUsuario());
        textoFecha.setText(item.getFecha());
        textoDescripcion.setText(item.getDescripcion());

        return view;
    }

    public List<Publicacion> parseJson(JSONObject jsonObject){
        // Variables locales
        List<Publicacion> posts = new ArrayList<>();
        JSONArray jsonArray= null;

        try {
            // Obtener el array del objeto
            jsonArray = jsonObject.getJSONArray("items");

            for(int i=0; i<jsonArray.length(); i++){

                try {
                    JSONObject objeto= jsonArray.getJSONObject(i);

                    Publicacion post = new Publicacion(
                            objeto.getString("usuario"),
                            objeto.getString("descripcion"),
                            objeto.getString("fecha"));


                    posts.add(post);

                } catch (JSONException e) {
                    Log.e(TAG, "Error de parsing: "+ e.getMessage());
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return posts;
    }
}*/

/*public class PublicacionAdapter extends ArrayAdapter {

    // Atributos
    private String URL_BASE = "http://192.168.122.1/casos/datos.json";
    private static final String TAG = "PostAdapter";
    ListaPublicaciones items;

    public PublicacionAdapter(Context context, int resource) {
        super(context, resource);

        // Añadir petición GSON a la cola
        ColaSingleton.getInstance(getContext()).addToRequestQueue(
                new GsonRequest<ListaPublicaciones>(
                        URL_BASE,
                        ListaPublicaciones.class,
                        null,
                        new Response.Listener<ListaPublicaciones>(){
                            @Override
                            public void onResponse(ListaPublicaciones response) {
                                items = response;
                                notifyDataSetChanged();
                            }
                        },
                        new Response.ErrorListener(){
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley:"+ error.getMessage());
                            }
                        }
                )

        );

        System.out.println(items);

    }

    @Override
    public int getCount() {
        return items != null ? items.getItems().size() : 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        //Comprobando si el View no existe

        if (view == null){
            view = layoutInflater.inflate(
                    R.layout.item_publicacion,
                    parent,
                    false);
        }


        // Obtener el item_publicacion actual
        Publicacion item = items.getItems().get(position);

        // Obtener Views
        TextView textoUsuario = (TextView) view.findViewById(R.id.usuario);
        TextView textoFecha = (TextView) view.findViewById(R.id.fecha);
        TextView textoDescripcion = (TextView) view.findViewById(R.id.descripcion);

        Log.d("user:",item.getUsuario());
        Log.d("fecha:",item.getFecha());
        Log.d("desc:",item.getDescripcion());

        // Actualizar los Views
        textoUsuario.setText(item.getUsuario());
        textoFecha.setText(item.getFecha());
        textoDescripcion.setText(item.getDescripcion());

        return view;
    }
}*/