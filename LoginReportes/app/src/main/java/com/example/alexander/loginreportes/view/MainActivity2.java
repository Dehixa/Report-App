package com.example.alexander.loginreportes.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.alexander.loginreportes.R;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button boton = (Button) findViewById(R.id.btnLista);

        boton.setOnClickListener(new View.OnClickListener() { // hago clic en el botón

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity2.this, Publicar.class);
                startActivity(intent); //abro la url en una nueva activity
            }
        });

    }
}

