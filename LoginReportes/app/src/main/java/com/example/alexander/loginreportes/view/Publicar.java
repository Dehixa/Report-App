package com.example.alexander.loginreportes.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import com.example.alexander.loginreportes.R;
import com.example.alexander.loginreportes.adapters.PublicacionAdapter;

public class Publicar extends AppCompatActivity {

    // Atributos
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicar);

        // Obtener instancia de la lista
        listView= (ListView) findViewById(R.id.lista);

        // Crear adaptador
        PublicacionAdapter adapter = new PublicacionAdapter(getApplicationContext());

        //listView.setAdapter(adapter);

    }
}
